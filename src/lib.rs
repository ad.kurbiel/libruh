use isahc::{HttpClient, HttpClientBuilder, Request, prelude::Configurable, config::RedirectPolicy, ReadResponseExt, Body, Response};
use model::{Grade, UserId, User, GradeId, Subject, SubjectId, LessonId, Lesson, Category, CategoryId};

pub mod model;
mod api;

pub struct Client {
    login: String,
    password: String,

    http: HttpClient,
}

impl Client {
    pub fn new(login: String, password: String) -> Self {
        let client = Self {
            login,
            password,
            http: HttpClientBuilder::new()
                .cookies()
                .build().unwrap(),
        };

        client.authenticate();

        client
    }

    pub fn get_grades(&self) -> Vec<Grade> {
        let mut r = self.http.get("https://synergia.librus.pl/gateway/api/2.0/Grades").unwrap();
        serde_json::from_value(
            serde_json::from_str::<serde_json::Value>(&r.text().unwrap()).unwrap()
                .get_mut("Grades").unwrap()
                .take()
        ).unwrap()
    }

    pub fn get_users(&self) -> Vec<User> {
        let mut r = self.http.get("https://synergia.librus.pl/gateway/api/2.0/Users").unwrap();
        serde_json::from_value(
            serde_json::from_str::<serde_json::Value>(&r.text().unwrap()).unwrap()
                .get_mut("Users").unwrap()
                .take()
        ).unwrap()
    }

    pub fn get_subjects(&self) -> Vec<Subject> {
        let mut r = self.http.get("https://synergia.librus.pl/gateway/api/2.0/Subjects").unwrap();
        serde_json::from_value(
            serde_json::from_str::<serde_json::Value>(&r.text().unwrap()).unwrap()
                .get_mut("Subjects").unwrap()
                .take()
        ).unwrap()
    }

    pub fn get_lessons(&self) -> Vec<Lesson> {
        let mut r = self.http.get("https://synergia.librus.pl/gateway/api/2.0/Lessons").unwrap();
        serde_json::from_value(
            serde_json::from_str::<serde_json::Value>(&r.text().unwrap()).unwrap()
                .get_mut("Lessons").unwrap()
                .take()
        ).unwrap()
    }

    pub fn get_categories(&self) -> Vec<Category> {
        let mut r = self.http.get("https://synergia.librus.pl/gateway/api/2.0/Grades/Categories").unwrap();
        serde_json::from_value(
            serde_json::from_str::<serde_json::Value>(&r.text().unwrap()).unwrap()
                .get_mut("Categories").unwrap()
                .take()
        ).unwrap()
    }

    pub fn request<B>(&self, request: Request<B>) -> Response<Body>
        where B: Into<Body>
    {
        self.http.send(request).unwrap()
    }

    fn authenticate(&self) {
        self.http.get("https://api.librus.pl/OAuth/Authorization?client_id=46&response_type=code&scope=mydata").unwrap();
        self.http.post("https://api.librus.pl/OAuth/Authorization?client_id=46", format!("action=login&login={}&pass={}", self.login, self.password)).unwrap();
        self.http.send(Request::get("https://api.librus.pl/OAuth/Authorization/Grant?client_id=46")
            .redirect_policy(RedirectPolicy::Follow)
            .body(()).unwrap()
        ).unwrap();
    }
}

impl GradeId {
    pub fn get(&self, client: &Client) -> Grade {
        let mut r = client.http.get(format!("https://synergia.librus.pl/gateway/api/2.0/Grades/{}", self.0)).unwrap();
        serde_json::from_value(
            serde_json::from_str::<serde_json::Value>(&r.text().unwrap()).unwrap()
                .get_mut("Grade").unwrap()
                .take()
        ).unwrap()
    }
}

impl UserId {
    pub fn get(&self, client: &Client) -> User {
        let mut r = client.http.get(format!("https://synergia.librus.pl/gateway/api/2.0/Users/{}", self.0)).unwrap();
        serde_json::from_value(
            serde_json::from_str::<serde_json::Value>(&r.text().unwrap()).unwrap()
                .get_mut("User").unwrap()
                .take()
        ).unwrap()
    }
}

impl SubjectId {
    pub fn get(&self, client: &Client) -> Subject {
        let mut r = client.http.get(format!("https://synergia.librus.pl/gateway/api/2.0/Subjects/{}", self.0)).unwrap();
        serde_json::from_value(
            serde_json::from_str::<serde_json::Value>(&r.text().unwrap()).unwrap()
                .get_mut("Subject").unwrap()
                .take()
        ).unwrap()
    }
}

impl LessonId {
    pub fn get(&self, client: &Client) -> Lesson {
        let mut r = client.http.get(format!("https://synergia.librus.pl/gateway/api/2.0/Lessons/{}", self.0)).unwrap();
        serde_json::from_value(
            serde_json::from_str::<serde_json::Value>(&r.text().unwrap()).unwrap()
                .get_mut("Lesson").unwrap()
                .take()
        ).unwrap()
    }
}

impl CategoryId {
    pub fn get(&self, client: &Client) -> Category {
        let mut r = client.http.get(format!("https://synergia.librus.pl/gateway/api/2.0/Grades/Categories/{}", self.0)).unwrap();
        serde_json::from_value(
            serde_json::from_str::<serde_json::Value>(&r.text().unwrap()).unwrap()
                .get_mut("Category").unwrap()
                .take()
        ).unwrap()
    }
}
