use chrono::{Date, NaiveDate, TimeZone};
use chrono_tz::{Tz, Europe::Warsaw};
use serde::Deserialize;

use crate::api;

#[derive(Debug)]
pub struct Grade {
    pub id: GradeId,
    pub lesson: LessonId,
    pub subject: SubjectId,
    pub student: UserId,
    pub category: CategoryId,
    pub teacher: UserId,
    pub grade: String,
    pub date: Date<Tz>,
    pub term: Term,
    pub improvement: Option<GradeId>,
    pub kind: GradeKind,
    pub comments: Vec<CommentId>,
}

impl<'de> Deserialize<'de> for Grade {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: serde::Deserializer<'de>
    {
        let api_grade = api::Grade::deserialize(deserializer)?;

        let date = Warsaw.from_local_date(
            &NaiveDate::parse_from_str(&api_grade.date, "%Y-%m-%d").map_err(serde::de::Error::custom)?
        ).single().unwrap();
        let term = match api_grade.semester {
            1 => Term::First,
            2 => Term::Second,
            _ => todo!(),
        };
        let kind = if api_grade.is_constituent {
            GradeKind::Component
        } else if api_grade.is_semester {
            GradeKind::Term
        } else if api_grade.is_semester_proposition {
            GradeKind::TermProposal
        } else if api_grade.is_final {
            GradeKind::Final
        } else if api_grade.is_final_proposition {
            GradeKind::FinalProposal
        } else {
            todo!();
        };
        let comments: Vec<CommentId> = api_grade.comments
            .into_iter()
            .map(|comment| CommentId(comment.id))
            .collect();

        Ok(Self {
            id: GradeId(api_grade.id),
            lesson: LessonId(api_grade.lesson.id),
            subject: SubjectId(api_grade.subject.id),
            student: UserId(api_grade.student.id),
            category: CategoryId(api_grade.category.id),
            teacher: UserId(api_grade.added_by.id),
            grade: api_grade.grade,
            date,
            term,
            improvement: api_grade.improvement.map(|n| GradeId(n.id)),
            kind,
            comments,
        })
    }
}

#[derive(Debug, Deserialize)]
pub struct GradeId(pub u32);

#[derive(Debug)]
pub enum Term {
    First,
    Second,
}

#[derive(Debug)]
pub enum GradeKind {
    Component,
    Term,
    TermProposal,
    Final,
    FinalProposal,
}

#[derive(Debug, Deserialize)]
pub struct SubjectId(pub u32);

#[derive(Debug)]
pub struct Subject {
    pub id: SubjectId,
    pub name: String,
    pub short: String,
}

impl<'de> Deserialize<'de> for Subject {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: serde::Deserializer<'de>
    {
        let api_subject = api::Subject::deserialize(deserializer)?;

        Ok(Self {
            id: SubjectId(api_subject.id),
            name: api_subject.name,
            short: api_subject.short,
        })
    }
}

#[derive(Debug)]
pub struct User {
    pub id: UserId,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
}

impl<'de> Deserialize<'de> for User {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: serde::Deserializer<'de>
    {
        let api_user = api::User::deserialize(deserializer)?;

        Ok(Self {
            id: UserId(api_user.id),
            first_name: api_user.first_name,
            last_name: api_user.last_name,
        })
    }
}

#[derive(Debug, Deserialize)]
pub struct UserId(pub u32);

#[derive(Debug)]
pub struct Lesson {
    pub id: LessonId,
    pub teacher: UserId,
    pub subject: SubjectId,
    pub class: Option<ClassId>,
}

impl<'de> Deserialize<'de> for Lesson {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: serde::Deserializer<'de>
    {
        let api_lesson = api::Lesson::deserialize(deserializer)?;

        Ok(Self {
            id: LessonId(api_lesson.id),
            teacher: UserId(api_lesson.teacher.id),
            subject: SubjectId(api_lesson.subject.id),
            class: api_lesson.class.map(|n| ClassId(n.id)),
        })
    }
}

#[derive(Debug, Deserialize)]
pub struct LessonId(pub u32);

#[derive(Debug)]
pub struct Category {
    pub id: CategoryId,
    pub teacher: Option<UserId>,
    pub color: ColorId,
    pub name: String,
    pub short: Option<String>,
    pub scale: Option<ScaleId>,
    pub weight: Option<u8>,
    pub parent: Option<CategoryId>,
}

impl<'de> Deserialize<'de> for Category {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: serde::Deserializer<'de>
    {
        let api_category = api::Category::deserialize(deserializer)?;

        Ok(Self {
            id: CategoryId(api_category.id),
            teacher: api_category.teacher.map(|n| UserId(n.id)),
            color: ColorId(api_category.color.id),
            name: api_category.name,
            short: api_category.short,
            weight: api_category.weight,
            parent: api_category.parent_category.map(|n| CategoryId(n.id)),
            scale: api_category.scale.map(|n| ScaleId(n.id))
        })
    }
}

#[derive(Debug, Deserialize)]
pub struct CategoryId(pub u32);

#[derive(Debug, Deserialize)]
pub struct CommentId(pub u32);

#[derive(Debug, Deserialize)]
pub struct ClassId(pub u32);

#[derive(Debug, Deserialize)]
pub struct ColorId(pub u32);

#[derive(Debug, Deserialize)]
pub struct ScaleId(pub u32);
