use serde::Deserialize;

#[derive(Deserialize)]
pub struct NestedId {
    #[serde(rename = "Id")]
    pub id: u32,
}

#[derive(Deserialize)]
pub struct Grade {
    #[serde(rename = "Id")]
    pub id: u32,
    #[serde(rename = "Lesson")]
    pub lesson: NestedId,
    #[serde(rename = "Subject")]
    pub subject: NestedId,
    #[serde(rename = "Student")]
    pub student: NestedId,
    #[serde(rename = "Category")]
    pub category: NestedId,
    #[serde(rename = "AddedBy")]
    pub added_by: NestedId,
    #[serde(rename = "Grade")]
    pub grade: String,
    #[serde(rename = "Date")]
    pub date: String,
    #[serde(rename = "AddDate")]
    pub add_date: String,
    #[serde(rename = "Semester")]
    pub semester: u8,
    #[serde(rename = "Improvement")]
    pub improvement: Option<NestedId>,
    #[serde(rename = "Resit")]
    pub resit: Option<NestedId>,
    #[serde(rename = "IsConstituent")]
    pub is_constituent: bool,
    #[serde(rename = "IsSemester")]
    pub is_semester: bool,
    #[serde(rename = "IsSemesterProposition")]
    pub is_semester_proposition: bool,
    #[serde(rename = "IsFinal")]
    pub is_final: bool,
    #[serde(rename = "IsFinalProposition")]
    pub is_final_proposition: bool,
    #[serde(rename = "Comments")]
    #[serde(default)]
    pub comments: Vec<NestedId>,
}

#[derive(Deserialize)]
pub struct Subject {
    #[serde(rename = "Id")]
    pub id: u32,
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "No")]
    pub no: u32,
    #[serde(rename = "Short")]
    pub short: String,
    #[serde(rename = "IsExtracurricular")]
    pub is_extracurricular: bool,
    #[serde(rename = "IsBlockLesson")]
    pub is_block_lesson: bool,
}

#[derive(Deserialize)]
pub struct User {
    #[serde(rename = "Id")]
    pub id: u32,
    #[serde(rename = "AccountId")]
    pub account_id: String,
    #[serde(rename = "FirstName")]
    pub first_name: Option<String>,
    #[serde(rename = "LastName")]
    pub last_name: Option<String>,
    #[serde(rename = "IsSchoolAdministrator")]
    #[serde(default)]
    pub is_school_administrator: bool,
    #[serde(rename = "IsEmployee")]
    pub is_employee: bool,
    #[serde(rename = "GroupId")]
    pub group_id: u32,
}

#[derive(Deserialize)]
pub struct Lesson {
    #[serde(rename = "Id")]
    pub id: u32,
    #[serde(rename = "Teacher")]
    pub teacher: NestedId,
    #[serde(rename = "Subject")]
    pub subject: NestedId,
    #[serde(rename = "Class")]
    pub class: Option<NestedId>,
}

#[derive(Deserialize)]
pub struct Category {
    #[serde(rename = "Id")]
    pub id: u32,
    #[serde(rename = "Teacher")]
    pub teacher: Option<NestedId>,
    #[serde(rename = "Color")]
    pub color: NestedId,
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "AdultsExtramural")]
    pub adults_extramural: bool,
    #[serde(rename = "AdultsDaily")]
    pub adults_daily: bool,
    #[serde(rename = "Standard")]
    pub standard: bool,
    #[serde(rename = "IsReadOnly")]
    pub is_read_only: String,
    #[serde(rename = "CountToTheAverage")]
    pub count_to_the_average: bool,
    #[serde(rename = "Weight")]
    pub weight: Option<u8>,
    #[serde(rename = "Short")]
    pub short: Option<String>,
    #[serde(rename = "Scale")]
    pub scale: Option<NestedId>,
    #[serde(rename = "ParentCategory")]
    pub parent_category: Option<NestedId>,
    #[serde(rename = "BlockAnyGrades")]
    pub block_any_grades: bool,
    #[serde(rename = "ObligationToPerform")]
    pub obligation_to_perform: bool,
}
